#!/bin/bash

## list of remote servers
declare -a remotehostarray=("YOUR.REMOTE.HOSTNAME" "YOUR.ALTERNATE.HOSTNAME")

for host in "${remotehostarray[@]}"
  do

  echo "checking host $host..."

  ## modify /etc/blah to disable ipv6 results from getent (would need to modify regex below to support IPV6 remotes)
  dns1=$(getent hosts $host | awk '{ print $1}')

  ## possible remote SSH ports to attempt - examples below
  declare -a portarray=("22" "8080" "10000")

  ## dns resolves to IPv4 Address?
    if $(grep -q "^\([1-9]\?[0-9]\|1[0-9][0-9]\|2\([0-4][0-9]\|5[0-5]\)\.\)\{3\}[1-9]\?[0-9]\|1[0-9][0-9]\|2\([0-4][0-9]\|5[0-5]\)" <<< $dns1); then
  
     for i in "${portarray[@]}"
     do
         echo "trying port $i..."
         /usr/bin/ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i [[[YOUR_PATH_AND_SSH_KEY]]] -l [[[YOUR_REMOTE_USER]]] -p $i $dns1 -NR [[[YOUR_REMOTE_PORT]]]:[[[YOUR.LOCAL.IP.ADDRESS]]]:[[[YOUR_LOCAL_PORT]]]
     done

  else
  	echo "couldn't resolve $host!"
  	exit
  fi

done

